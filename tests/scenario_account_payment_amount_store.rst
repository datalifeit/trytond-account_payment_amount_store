=====================================
Account Payment Amount Store Scenario
=====================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts
    >>> from decimal import Decimal
    >>> import datetime

Install account_payment_amount_store::

    >>> config = activate_modules('account_payment_amount_store')

Create tomorrow date::

    >>> tomorrow = datetime.date.today() + datetime.timedelta(days=1)

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = create_fiscalyear(company)
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> Journal = Model.get('account.journal')

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> payable = accounts['payable']
    >>> expense = accounts['expense']
    >>> expense_journal, = Journal.find([('code', '=', 'EXP')])

Create payment journal::

    >>> PaymentJournal = Model.get('account.payment.journal')

    >>> payment_journal = PaymentJournal()
    >>> payment_journal.name = 'Manual'
    >>> payment_journal.process_method = 'manual'
    >>> payment_journal.save()

Create parties::

    >>> Party = Model.get('party.party')

    >>> supplier = Party(name='Supplier')
    >>> supplier.save()

Create payable move::

    >>> Move = Model.get('account.move')
    
    >>> move = Move()
    >>> move.journal = expense_journal

    >>> line = move.lines.new()
    >>> line.account = payable
    >>> line.party = supplier
    >>> line.credit = Decimal('50.00')

    >>> line2 = move.lines.new()
    >>> line2.account = expense
    >>> line2.debit = Decimal('50.00')
    >>> move.click('post')

    >>> line_payable, = [l for l in move.lines if l.account == payable]
    >>> line_payable.payment_amount_store
    Decimal('50.00')
    >>> line_payable.payment_amount
    Decimal('50.00')

Partially pay line::

    >>> Payment = Model.get('account.payment')

    >>> wizard = Wizard('account.move.line.pay', [line_payable])
    >>> wizard.form.date = tomorrow
    >>> wizard.execute('next_')
    >>> wizard.execute('next_')

    >>> payment, = Payment.find()
    >>> payment.amount
    Decimal('50.00')
    >>> line_payable.payment_amount_store
    Decimal('0.00')
    >>> line_payable.payment_amount
    Decimal('0.00')

    >>> payment.amount = Decimal('45.00')
    >>> payment.save()
    >>> line_payable.reload()
    >>> line_payable.payment_amount_store
    Decimal('5.00')
    >>> line_payable.payment_amount
    Decimal('5.00')

    >>> payment.delete()
    >>> line_payable.reload()
    >>> line_payable.payment_amount_store
    Decimal('50.00')
    >>> line_payable.payment_amount
    Decimal('50.00')